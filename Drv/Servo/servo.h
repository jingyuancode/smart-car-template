#ifndef __SERVO_H__
#define __SERVO_H__

#include "main.h"

void servo_init(TIM_HandleTypeDef* htim, uint32_t channel, int32_t min, int32_t max);
void servo_set(float degree);

#endif