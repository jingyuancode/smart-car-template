#include "inductor.h"

uint16_t inductor_raw[4] = {};

void inductor_init(ADC_HandleTypeDef* hadc)
{
	HAL_ADC_Start_DMA(hadc, (uint32_t*)&inductor_raw, 4);
}