#ifndef __MOTOR_H__
#define __MOTOR_H__

#include "main.h"

void motor_init(TIM_HandleTypeDef* htim, uint32_t channel_left_1, uint32_t channel_left_2, uint32_t channel_right_1, uint32_t channel_right_2, int32_t max);
void motor_set(float speed_left, float speed_right);

#endif